extern crate walkdir;
extern crate threadpool;
extern crate sha3;

use std::collections::HashMap;
use std::{fs, io};
use std::sync::mpsc::channel;

use walkdir::WalkDir;

use sha3::{Digest, Keccak512};

fn main() {
    let search_paths = {
        let mut list: Vec<_> = std::env::args().skip(1).collect();
        if list.len() == 0 {
            list.push(".".into());
        }
        list
    };
    println!("search_paths: {:?}", search_paths);
    let (tx, rx) = channel();
    let pool = threadpool::Builder::new().build();

    let pool_i = pool.clone();
    pool.execute(move || {
        let walker = search_paths.iter().flat_map(|path| WalkDir::new(path).follow_links(false).same_file_system(true));
        for entry in walker.filter_map(|e| e.ok()) {
            if entry.path().is_file() {
                let tx = tx.clone();
                pool_i.execute(move || {
                    let mut file = fs::File::open(entry.path()).expect(&format!("worker is unable to open file: {}", entry.path().display()));
                    let mut hasher = Keccak512::new();
                    let _bytes_copied = io::copy(&mut file, &mut hasher).unwrap();
                    let hash = hasher.result();
                    tx.send((entry.path().to_path_buf(), hash)).unwrap();
                });
            }
        }
    });

    let mut map = HashMap::with_capacity(1_000_000);
    for (path, hash) in rx.iter() {
        let list = map.entry(hash).or_insert_with(|| vec![]);
        list.push(path);
    }

    map.retain(|&_hash, &mut ref paths| paths.len() > 1);

    for (hash, paths) in &map {
        println!("\n{:x}", hash);
        for path in paths {
            println!("    {}", path.display());
        }
    }

    pool.join()
}
